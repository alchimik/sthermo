MODULE thermlib
  ! ----------------------------------------------------------------------------
  !  CALCULATE FREE ENERGY CORRECTION USING FREQUENCIES, GIVEN IN INPUT
  ! 
  !  modes : number of input modes
  !  temp  : applied temperature
  !  shift : shift of low frequency modes (also applied to imaginary modes)
  !  freq  : frequency in cm-1
  ! ----------------------------------------------------------------------------
  IMPLICIT NONE
  PRIVATE
  INTEGER, PARAMETER, PUBLIC            :: dp = selected_real_kind( 15 ,  307 )
  
  ! units
  REAL(dp) , PARAMETER, PUBLIC :: kB  = 8.6173303D-5       ! eV/K
  REAL(dp) , PARAMETER, PUBLIC :: hbar= 6.582119514D-16    ! eV*s
  REAL(dp) , PARAMETER, PUBLIC :: pi  = 3.14159265358979323846
  REAL(dp) , PARAMETER, PUBLIC :: NA  = 6.022140857D23
  REAL(dp) , PARAMETER, PUBLIC :: eVJ = 1.6021765D-19

  INTEGER, PARAMETER                    :: list_length=111 , sym_length=2
  CHARACTER(LEN=sym_length) , PARAMETER :: anon_element='Xx'
  REAL(dp)         , PARAMETER          :: anon_weight =0.0D0
  CHARACTER(LEN=sym_length)             :: list_symbols(list_length)
  REAL(dp)                              :: list_weights(list_length)

  DATA list_weights / &
    1.0079D0,   4.0026D0,   6.9410D0,   9.0122D0,  10.8110D0,  12.0107D0,  14.0067D0,  15.9994D0,  18.9984D0,  20.1797D0,&
   22.9897D0,  24.3050D0,  26.9815D0,  28.0855D0,  30.9738D0,  32.0650D0,  35.4530D0,  39.9480D0,  39.0983D0,  40.0780D0,&
   44.9559D0,  47.8670D0,  50.9415D0,  51.9961D0,  54.9380D0,  55.8450D0,  58.9332D0,  58.6934D0,  63.5460D0,  65.3900D0,&
   69.7230D0,  72.6400D0,  74.9216D0,  78.9600D0,  79.9040D0,  83.8000D0,  85.4678D0,  87.6200D0,  88.9059D0,  91.2240D0,&
   92.9064D0,  95.9400D0,  98.0000D0, 101.0700D0, 102.9055D0, 106.4200D0, 107.8682D0, 112.4110D0, 114.8180D0, 118.7100D0,&
  121.7600D0, 127.6000D0, 126.9045D0, 131.2930D0, 132.9055D0, 137.3270D0, 138.9055D0, 140.1160D0, 140.9077D0, 144.2400D0,&
  145.0000D0, 150.3600D0, 151.9640D0, 157.2500D0, 158.9253D0, 162.5000D0, 164.9303D0, 167.2590D0, 168.9342D0, 173.0400D0,&
  174.9670D0, 178.4900D0, 180.9479D0, 183.8400D0, 186.2070D0, 190.2300D0, 192.2170D0, 195.0780D0, 196.9665D0, 200.5900D0,&
  204.3833D0, 207.2000D0, 208.9804D0, 209.0000D0, 210.0000D0, 222.0000D0, 223.0000D0, 226.0000D0, 227.0000D0, 232.0381D0,&
  231.0359D0, 238.0289D0, 237.0000D0, 244.0000D0, 243.0000D0, 247.0000D0, 247.0000D0, 251.0000D0, 252.0000D0, 257.0000D0,&
  258.0000D0, 259.0000D0, 262.0000D0, 261.0000D0, 262.0000D0, 266.0000D0, 264.0000D0, 277.0000D0, 268.0000D0, 270.0000D0,&
  272.0000D0 /

  DATA list_symbols / &
    'H ','He','Li','Be','B ','C ','N ','O ','F ','Ne',&
    'Na','Mg','Al','Si','P ','S ','Cl','Ar','K ','Ca',&
    'Sc','Ti','V ','Cr','Mn','Fe','Co','Ni','Cu','Zn',&
    'Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y ','Zr',&
    'Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn',&
    'Sb','Te','I ','Xe','Cs','Ba','La','Ce','Pr','Nd',&
    'Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb',&
    'Lu','Hf','Ta','W ','Re','Os','Ir','Pt','Au','Hg',&
    'Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th',&
    'Pa','U ','Np','Pu','Am','Cm','Bk','Cf','Es','Fm',&
    'Md','No','Lr','Rf','Db','Sg','Bh','Hs','Mt','Ds',&
    'Rg'/

  TYPE atom
    INTEGER  :: e
    REAL(dp) :: m
    REAL(dp) :: r(3)
  END TYPE atom
  
  PUBLIC :: atom
  PUBLIC :: atomic_number
  PUBLIC :: element_weight
  PUBLIC :: xyz_read , freq_read
  PUBLIC :: calc_trans , calc_rot , calc_vib
  PUBLIC :: parse_query

  ! 3D diagonalization
  INTERFACE
    SUBROUTINE dsyevj3( A , W )
      INTEGER, PARAMETER        :: dp = selected_real_kind( 15 ,  307 )
      REAL(dp) , INTENT(INOUT)  :: A(3,3)   ! symmetric input matrix
      REAL(dp) , INTENT(OUT)    :: W(3)     ! EIGENVALUES
    END SUBROUTINE dsyevj3
  END INTERFACE
  
 CONTAINS

  ELEMENTAL PURE FUNCTION atomic_number( ielement_symbol )
  ! Returns atomic number of element given by symbol
    INTEGER                                 :: atomic_number
    CHARACTER(LEN=sym_length) , INTENT(IN)  :: ielement_symbol
    CHARACTER(LEN=sym_length)               :: int_element_symbol   ! internal modifyable symbol
    INTEGER                                 :: counter , tmp

    int_element_symbol = ielement_symbol
    CALL clean_symbol( int_element_symbol )

    atomic_number = 0
    DO counter=1,list_length
     IF( int_element_symbol == list_symbols(counter) ) THEN
       atomic_number = counter
       RETURN
     END IF
    END DO
  END FUNCTION atomic_number

  ELEMENTAL PURE FUNCTION element_weight( iatomic_number )
    ! Returns element symbol, of element with input atomic number
    REAL(dp)                        :: element_weight
    INTEGER, INTENT(IN)             :: iatomic_number

    IF( iatomic_number <= 0 .OR. iatomic_number > list_length ) THEN
      element_weight = anon_weight
    ELSE
      element_weight = list_weights( iatomic_number )
    END IF
  END FUNCTION element_weight

  ELEMENTAL SUBROUTINE clean_symbol( ioelement_symbol )
    ! First letter upper case, second letter lower case
    CHARACTER(LEN=sym_length), INTENT(INOUT) :: ioelement_symbol
    INTEGER                         :: counter , tmp
    LOGICAL                         :: integrity

    ! worst case
    integrity = .FALSE.

    ! convert first letter to upper case
    tmp = IACHAR(ioelement_symbol(1:1))
    IF( tmp > 96 .AND. tmp <= 122 ) THEN
      ioelement_symbol(1:1) = ACHAR(tmp-32)
    ELSE IF( tmp < 64 .AND. tmp >= 90 ) THEN
      ioelement_symbol(1:1) = anon_element(1:1)
    END IF
    ! convert second letter to lower case
    tmp = IACHAR(ioelement_symbol(2:2))
    IF( tmp > 64 .AND. tmp <= 90 ) THEN
      ioelement_symbol(1:1) = ACHAR(tmp+32)
    ELSE IF( tmp < 96 .AND. tmp >= 122 ) THEN
      ioelement_symbol(2:2) = anon_element(2:2)
    END IF
    ! check if the element exists
    DO counter=1,list_length
      IF( ioelement_symbol == list_symbols(counter) ) THEN
        integrity = .TRUE.
        EXIT
      END IF
    END DO
    IF( .NOT. integrity ) THEN
      ioelement_symbol = anon_element
    END IF
  END SUBROUTINE clean_symbol


  PURE SUBROUTINE mass_center_mol( atm )
    TYPE(atom) , INTENT(INOUT)  :: atm(:)

    REAL(dp) :: center(3) , mass
    INTEGER  :: i

    center(:) = 0.0D0
    mass      = 0.0D0

    DO i=1,SIZE(atm)
      center(:) = center(:) + atm(i)%m * atm(i)%r(:)
      mass      = mass      + atm(i)%m
    ENDDO

    center(:)  = center(:) / mass

    ! shift
    DO i=1,SIZE(atm)
      atm(i)%r(:) = atm(i)%r(:) - center(:)
    ENDDO
  END SUBROUTINE mass_center_mol


  SUBROUTINE freq_read( file , freq , stat )
    INTEGER  , INTENT(IN)                :: file
    REAL(dp) , INTENT(OUT) , ALLOCATABLE :: freq(:)
    INTEGER  , INTENT(OUT)               :: stat

    INTEGER  :: i , nfr

    IF( ALLOCATED(freq) ) DEALLOCATE( freq )

    READ(5,*,iostat=stat) nfr
    IF( stat /= 0 .OR. nfr > 5000 .OR. nfr <= 0 ) THEN
      WRITE(0,*) stat , "Error reading number of modes! Number now:", nfr
      IF( stat == 0 ) stat = -42
    END IF

    ALLOCATE( freq(nfr) )

    DO i = 1,nfr
      READ(5,*,iostat=stat) freq(i)
      IF( stat /= 0 ) WRITE(0,'(I5,1X,A,I5,A)') stat , "Error reading frequency",i,"!"
    ENDDO
  END SUBROUTINE freq_read


  SUBROUTINE xyz_read( file , atm , stat )
    INTEGER    , INTENT(IN)                :: file
    TYPE(atom) , INTENT(OUT) , ALLOCATABLE :: atm(:)
    INTEGER    , INTENT(OUT)               :: stat

    INTEGER          :: i , nat
    CHARACTER(LEN=2) :: comment , symbol

    ! read in xyz
    READ(5,*,iostat=stat) nat
    IF( stat /= 0 ) WRITE(0,'(I5,1X,A)') stat , "Error reading number of atoms!"

    IF( ALLOCATED(atm) ) DEALLOCATE( atm )
    ALLOCATE( atm(nat) )

    ! read comment line
    READ(5,'(A)',iostat=stat) comment
    IF( stat > 0 ) WRITE(0,'(I5,1X,A)') stat , "Error in comment line!"

    ! read atoms
    DO i=1,nat
      READ(5,*,iostat=stat) symbol , atm(i)%r(:)
      IF( stat /= 0 ) WRITE(0,'(I5,1X,A,I4,A)') stat , "Error reading atom", i ,"!"
      atm(i)%e = atomic_number( symbol )
      atm(i)%m = element_weight( atm(i)%e )
    END DO
  END SUBROUTINE xyz_read

  
  SUBROUTINE calc_trans( mass , temp , pres , etrans , ctrans , strans )
    ! --------------------------------------------------------------------------
    ! calculate translational terms
    !
    ! Units:
    ! ======
    ! mass in g/mol
    ! temp(erature) in Kelvin
    ! pres(sure) in bar
    !
    ! output in eV and eV/K
    ! --------------------------------------------------------------------------

    REAL(dp) , INTENT(IN)    :: mass , temp , pres
    REAL(dp) , INTENT(OUT)   :: etrans , ctrans , strans

    REAL(dp) :: pV , kBT , m , qtrans
    INTEGER  :: i

    m    = mass / NA / 1000.0D0
  
       kBT = kB*temp
        pV = kBT         ! free particle

    qtrans = SQRT( ( m * kBT /(2.0D0 * pi * hbar**2) )**3 / eVJ ) * kBT / pres * 1.0D-5

    etrans = 1.5D0 * kBT + pV
    ctrans = 1.5D0 * kB
    strans = kB * ( LOG(qtrans) + 2.5D0 )

  END SUBROUTINE calc_trans


  SUBROUTINE calc_rot( atm , symm , temp , erot , crot , srot )
    ! --------------------------------------------------------------------------
    ! calculate rotational terms
    !
    ! Units:
    ! ======
    ! atm:  mass in g/mol, coordinates in angstrom
    ! symm: dimensionless symmetry factor
    ! temp(perature) in Kelvin
    !
    ! output in eV and eV/K
    ! --------------------------------------------------------------------------
    
    REAL(dp)   , PARAMETER     :: rconmax=5.0D12 ! maximum rotational constant

    TYPE(atom) , INTENT(INOUT) :: atm(:)
    INTEGER    , INTENT(IN)    :: symm
    REAL(dp)   , INTENT(IN)    :: temp
    REAL(dp)   , INTENT(OUT)   :: erot , crot , srot
    
    REAL(dp)   :: trot(3,3) , rot(3) , rcon(3) , qrot , kBT
    INTEGER    :: i
    

    ! centralize and convert
    CALL mass_center_mol( atm )
    
    trot(:,:) = 0.0D0
    DO i = 1,SIZE(atm)
      
      ! convert to SI
      atm(i)%r(:) = atm(i)%r(:) * 1.0D-10     ! Angstrom to meter molecule in m
      atm(i)%m    = atm(i)%m    / NA * 1.0D-3 ! g/mol to kg

      ! moment of inertia
      trot(1,1) = trot(1,1)  +  atm(i) % m  * ( atm(i) % r(2)**2  +  atm(i) % r(3)**2 )
      trot(2,2) = trot(2,2)  +  atm(i) % m  * ( atm(i) % r(1)**2  +  atm(i) % r(3)**2 )
      trot(3,3) = trot(3,3)  +  atm(i) % m  * ( atm(i) % r(1)**2  +  atm(i) % r(2)**2 )
      trot(1,2) = trot(1,2)  +  atm(i) % m  *   atm(i) % r(1)    *   atm(i) % r(2)
      trot(1,3) = trot(1,3)  +  atm(i) % m  *   atm(i) % r(1)    *   atm(i) % r(3)
      trot(2,3) = trot(2,3)  +  atm(i) % m  *   atm(i) % r(2)    *   atm(i) % r(3)

    ENDDO

    trot(2,1) = trot(1,2)
    trot(3,1) = trot(1,3)
    trot(3,2) = trot(2,3)

    ! diagonalize tensor to get rotations
    !!! CHECK IF ZERO TENSOR NEEDS TO BE TREATED DIFFERENTLY
    CALL dsyevj3( trot(:,:) , rot(:) )
  
    ! calculate rotational constants
    rcon(:) = hbar * eVJ / ( 4.0D0 * pi ) / rot(:)
  
    ! calculate rotational contributions (check M. Hollas, Modern Spectroscopy, p. 113 + gaussian.com/thermo)
    !!! IMPLEMENT EQUATIONS FOR LINEAR CASE
     kBT = kB  * temp
    erot = kBT * 1.5D0
    crot = kB  * 1.5D0

    qrot = SQRT( ( 2.0D0*kBT / (hbar**2*eVJ) )**3 * pi * rot(1)*rot(2)*rot(3) ) / REAL(symm)
    srot = kB * ( LOG(qrot) + 1.5D0 )

    !WRITE(6,"('Rotational constants   [GHz]: ',3EN15.3)") rcon(:)*1D-9
  END SUBROUTINE calc_rot


  SUBROUTINE calc_vib( freq , temp , evib , cvib , svib , zpe , shift )
    ! --------------------------------------------------------------------------
    ! calculate vibrational terms
    !
    ! Units:
    ! ======
    ! freq(uencies) in reciprocal centimeters
    ! temp(perature) in Kelvin
    !
    ! output in eV and eV/K
    ! --------------------------------------------------------------------------
    REAL(dp) , PARAMETER     :: eVrcm = 8.06554D3 ! wavenumber conversion factor
    REAL(dp) , INTENT(IN)    :: freq(:)
    REAL(dp) , INTENT(IN)    :: temp
    REAL(dp) , INTENT(IN)  , OPTIONAL :: shift
    REAL(dp) , INTENT(OUT)   :: evib , cvib , svib
    REAL(dp) , INTENT(OUT) , OPTIONAL :: zpe
    REAL(dp) :: kBT , hfreq , expterm , quotient , diff
    REAL(dp) :: zpe_ , shf
    INTEGER  :: i

    shf = 20.0D0
    IF( PRESENT(shift) ) shf = shift

    evib   = 0.0D0
    cvib   = 0.0D0
    svib   = 0.0D0
    zpe_   = 0.0D0

    DO i = 1,SIZE(freq)
      hfreq = freq(i)

      ! shift low frequencies to whatever (in cm-1)
      IF( freq(i) < shf ) THEN
        WRITE(0,'(A,I4,A,F6.1,A,I4,A)') "WARNING! Mode",i," (",freq(i),") is smaller than",INT(shf)," and thus will be shifted."
        hfreq = shf 
      END IF

      kBT      = kB*temp
      hfreq    = hfreq / eVrcm        ! convert to eV
      expterm  = EXP( hfreq / kBT )
      quotient = hfreq / ( expterm - 1.0D0 )
      diff     = expterm ** (-1) - 1.0D0

      zpe_  = zpe_ + hfreq/2.0D0
      evib  = evib + hfreq/2.0D0   + quotient

      ! Cv*kB*T^2, Sv*T --> convert after summation
      cvib  = cvib + expterm * quotient*quotient
      svib  = svib + quotient - kBT*LOG( -diff )
      
    ENDDO

    cvib = cvib /temp/kBT
    svib = svib /temp

    IF( PRESENT(zpe) ) zpe = zpe_

  END SUBROUTINE calc_vib


  ! ----------------------------------------------------------------------------
  SUBROUTINE parse_query( query , command , options , stat )
    ! --------------------------------------------------------------------------
    ! query:    formatted string of commands and options
    ! command:  array of characters, each defining a single action
    ! options:  array of strings, processed by actions
    !
    ! A query is started by '%'. It follows a string of characters defining
    ! the command. After the command, options might follow, each separated by
    ! colons. I.e.
    ! I.e. '%abc' is a query of three actions a,b, and c without additional
    ! options. Queries can also be fairly complex:
    !
    !    %abcdef:12:3-8:POSCAR:0.74
    !
    ! This is a query of 6 options and 4 values, '12', '3-8', 'POSCAR' and
    ! '0.74'. Note, that all this values are saved as strings, and would need
    ! to be processed to be identified integers, floats, files, ranges, etc.
    !
    ! --------------------------------------------------------------------------
    CHARACTER(LEN=*) , INTENT(IN)  :: query
    CHARACTER(LEN=1) , ALLOCATABLE , INTENT(OUT) :: command(:)
    CHARACTER(LEN=:) , ALLOCATABLE , INTENT(OUT) :: options(:)
    INTEGER                        , INTENT(OUT) :: stat
    CHARACTER(LEN=:) , ALLOCATABLE :: iquery
    INTEGER :: i , j , k , l
    stat = 0
    iquery = TRIM(ADJUSTL(query))
    IF( iquery(1:1) /= '%' ) THEN
      WRITE(0,'(A)') "[parse query] ERROR: command does not start with '%'!"
      stat = 1
      RETURN
    ENDIF
    
    ! find first colon
    DO i=1,LEN(iquery)
      IF(iquery(i:i)==':') THEN
        EXIT
      END IF
    ENDDO
    k = i
    ! count colons
    l=0
    DO i=k,LEN(iquery)
      l = l + MERGE(1,0,iquery(i:i)==':')
    ENDDO
    ! allocate
    IF( ALLOCATED (command) ) DEALLOCATE(command)
    IF( ALLOCATED (options) ) DEALLOCATE(options)
    ALLOCATE( command(k-2) )
    ALLOCATE( CHARACTER(LEN = LEN(iquery)-l-k+1) :: options(l) )
    ! fill
    DO i=2,k-1
      command(i-1) = iquery(i:i)
    ENDDO
    j = 1
    l = 0
    DO i=k+1,LEN(iquery)
      IF( iquery(i:i)==':' ) THEN
        options(j) = iquery(i-l:i-1)
        j = j+1
        l = 0
      ELSE
        l = l+1
      END IF
    ENDDO
    options(j) = iquery(i-l:)
  END SUBROUTINE parse_query
  ! ------------------------------------------------------------------------ END


END MODULE thermlib



PROGRAM therm
  USE thermlib

  IMPLICIT NONE

  ! external quantities
  REAL(dp) :: temp=298.15 , pres=1.01325 , shift=20.0

  TYPE(atom) , ALLOCATABLE :: atm(:)
  REAL(dp) , ALLOCATABLE :: freq(:)

  ! switches
  LOGICAL  :: lxyz = .FALSE.
  LOGICAL  :: ldel = .FALSE.
  LOGICAL  :: lverbose = .TRUE.

  INTEGER  :: sym  = 1           ! symmetry factor
  REAL(dp) :: aweight , mweight  ! molecule mass

  ! scratch (vib sum)
  CHARACTER(LEN=2) :: symbol , comment

  ! output
  !REAL(dp) :: qtrans , qrot
  REAL(dp) :: etrans , erot , evib
  REAL(dp) :: strans , srot , svib
  REAL(dp) :: ctrans , crot , cvib
  REAL(dp) ::                 zpe
  REAL(dp) :: enthalpy , heatcap , entropy , gibbs
  
  REAL(dp) :: x

  ! command line
  CHARACTER(LEN=256) :: exe , arg
  REAL(dp) :: dummyfloat
  INTEGER  :: dummyint
  CHARACTER(LEN=1) , ALLOCATABLE :: command(:)
  CHARACTER(LEN=:) , ALLOCATABLE :: options(:)

  ! loop
  INTEGER  :: i , k , stat

  ! defaults
  CALL zero_data()

  lxyz  = .FALSE.
  ldel  = .FALSE.
  temp  = 298.15D0
  pres  = 1.01325D0
  shift = 20.0D0

  ! ----------------------------------------------------------------------------
  ! parse command line
  ! ----------------------------------------------------------------------------
  IF( COMMAND_ARGUMENT_COUNT() == 0 ) THEN
    ! set default behaviour
    arg = '%v'
  ELSE
    CALL GET_COMMAND_ARGUMENT(1,arg,status=stat)
  ENDIF
  
  IF( .NOT. arg(1:1) == '%' ) THEN
  ! help message
    CALL GET_COMMAND_ARGUMENT(0,exe)
    WRITE(6,'(A)') "Calculate thermodynamic data."
    WRITE(6,'(A)') "Environmental quantities are given as command line options. System data is read"
    WRITE(6,'(A)') "from standard input."
    WRITE(6,'(A)') ""
    WRITE(6,'(A)') "First line gives system information - (gas phase) molecule or condensed matter."
    WRITE(6,'(A)') ""
    WRITE(6,'(A)') "For condensed systems only vibrations are calculated. Frequencies are read line"
    WRITE(6,'(A)') "by line from standard input, first number is interpreted as number of frequencies."
    WRITE(6,'(A)') ""
    WRITE(6,'(A)') "For molecules the geometry is expected in .xyz format after the frequencies."
    WRITE(6,'(A)') "Temperature is expected in Kelvin, pressure in bar, and frequencies in cm-1."
    WRITE(6,'(A)') ""
    WRITE(6,'(A)') "Usage: "//TRIM(exe)//" [%<query>]"
    WRITE(6,'(A)') ""
    WRITE(6,'(A)') "Example input:"
    WRITE(6,'( )')
    WRITE(6,'(A)') "   Molecule 2"
    WRITE(6,'(A)') "   3"
    WRITE(6,'(A)') "   3984.9"
    WRITE(6,'(A)') "   3875.1"
    WRITE(6,'(A)') "   1624.0"
    WRITE(6,'(A)') "   3"
    WRITE(6,'(A)') "   water.xyz"
    WRITE(6,'(A)') "   O -0.90346  0.91257  0.48064"
    WRITE(6,'(A)') "   H  0.05352  0.91901  0.44752"
    WRITE(6,'(A)') "   H -1.19064  1.08701 -0.41604"
    WRITE(6,'( )')
    WRITE(6,'(A)') "Query:  Sequence of single character commands started by '%'. Values can be"
    WRITE(6,'(A)') "        given as colon-separated list after commands."
    WRITE(6,'( )')
    WRITE(6,'(A)') "Input commands:  t, p, m, d  for temperature, pressure and minimum frequency."
    WRITE(6,'(A)') "                 Shift lower frequencies by default, use d to delete."
    WRITE(6,'( )')
    WRITE(6,'(A)') "Output commands:  g, h, s, c, z  for Gibbs energy, Enthalpy,"
    WRITE(6,'(A)') "                  Entropy, Heat capacity, Zero-point energy."
    WRITE(6,'( )')
    WRITE(6,'(A)') "CAUTION: in case of errors, specified output is set to zero and still printed!"
    STOP 1

  ELSE
    CALL parse_query( arg , command , options , stat )
    IF( stat /= 0 ) THEN
      WRITE(0,'(A)') "Error while reading query."
    ENDIF
  ENDIF

  ! -----------------------------------------------------------
  ! parse query
  ! -----------------------------------------------------------
  ! check number of query arguments qarg
  k = 0
  DO i=1,SIZE(command)
    SELECT CASE( command(i) )
      CASE( 't' ) ; k = k+1
      CASE( 'p' ) ; k = k+1
      CASE( 'm' ) ; k = k+1
    END SELECT
  ENDDO

  IF( k /= SIZE(options) ) THEN
    WRITE(0,'(A,I2,A,I2,A)') "[template]    ERROR: Wrong number of options, needed:",k,", found:",SIZE(options),"!"
    STOP 255
  END IF

  k = 0
  DO i=1,SIZE(command)
    SELECT CASE( command(i) )

      CASE DEFAULT
        WRITE(0,"(A)") 'Option not recognized!'
        STOP 1

      CASE( 't' ) ; k = k+1 ; READ(options(k),*) temp
      CASE( 'p' ) ; k = k+1 ; READ(options(k),*) pres
      CASE( 'm' ) ; k = k+1 ; READ(options(k),*) shift
      CASE( 'd' ) ; ldel = .TRUE.

      CASE( 'c' ) ; CYCLE
      CASE( 'g' ) ; CYCLE
      CASE( 'h' ) ; CYCLE
      CASE( 's' ) ; CYCLE
      CASE( 'z' ) ; CYCLE
      CASE( 'v' ) ; CYCLE

    END SELECT
  ENDDO

  ! ----------------------------------------------------------------------------
  CALL calc_thermo()
  ! ----------------------------------------------------------------------------

  IF( stat /= 0 ) THEN
    CALL zero_data()
  END IF

  ! ----------------------------------------------------------------------------
  ! print output
  ! ----------------------------------------------------------------------------
  ! conversion factor, for debugging, or as quick hack
  ! units by default in eV and meV/K resp.
  x = 1.0D0
  !x = 96.485D0         ! convert to:   kJ/mol and   J/mol/K resp.
  !x = 96.485D0/4.184D0 ! convert to: kcal/mol and cal/mol/K resp.

  lverbose = .TRUE.
  DO i=1,SIZE(command)
    SELECT CASE( command(i) )

      CASE( 'c' )   ! heat capacity
        WRITE(6,"(F18.8)",advance='no') x*heatcap*1000
        lverbose = .FALSE.

      CASE( 'g' )   ! gibbs free energy
        WRITE(6,"(F18.8)",advance='no') x*gibbs
        lverbose = .FALSE.

      CASE( 'h' )   ! enthalpy
        WRITE(6,"(F18.8)",advance='no') x*enthalpy
        lverbose = .FALSE.

      CASE( 's' )   ! entropy
        WRITE(6,"(F18.8)",advance='no') x*entropy*1000
        lverbose = .FALSE.

      CASE( 'z' )   ! zero-point energy
        WRITE(6,"(F18.8)",advance='no') x*zpe
        lverbose = .FALSE.

      CASE( 'v' )   ! zero-point energy
        lverbose = .TRUE.
        EXIT

    END SELECT
  ENDDO

  IF( lverbose .AND. stat == 0 ) THEN
    WRITE(6,"()")
    WRITE(6,"('Temperature     [K]:',F9.2)")    temp
    WRITE(6,"('Pressure       [Pa]:',ES16.5)")  pres*100000
    WRITE(6,"('Num. of vib.  modes:',I6)")      SIZE(freq)
    WRITE(6,"('Lowest freq. [cm-1]: ',5F7.1)") freq(SIZE(freq  )) ,&
                                               freq(SIZE(freq)-1) ,&
                                               freq(SIZE(freq)-2) ,&
                                               freq(SIZE(freq)-3) ,&
                                               freq(SIZE(freq)-4)
    WRITE(6,"()")
    WRITE(6,"('              ',4A15)" ) 'H'   , 'CV'    , 'S'     , 'ZPE'
    WRITE(6,"('              ',4A15)" ) 'eV'  , 'meV/K' , 'meV/K' , 'eV'
    WRITE(6,"('Total         ',3F15.5)" ) x*enthalpy , -x*heatcap*1000  , x*entropy*1000
    WRITE(6,"()")
    !WRITE(6,"('              ',3A15)" ) '0.0    ','0.0    ','0.0    '
    WRITE(6,"('Translational ',3F15.5)" ) x*etrans   , -x*ctrans*1000  , x*strans*1000
    WRITE(6,"('Rotational    ',3F15.5)" ) x*erot     , -x*crot*1000    , x*srot*1000
    WRITE(6,"('Vibrational   ',4F15.5)" ) x*evib     , -x*cvib*1000    , x*svib*1000 , x*zpe
    WRITE(6,"()")
    WRITE(6,"('Gibbs free energy correction:',F15.5)" ) x*gibbs
    WRITE(6,"()")
  ELSE
    WRITE(6,"()")
  END IF

 ! -------------------------------------------------------------------------------------------------
 CONTAINS


  SUBROUTINE calc_thermo()
    CHARACTER(LEN=80) :: firstline
    CHARACTER(LEN=1) :: sys

    ! --------------------------------------------------------------------------
    ! initialize
    ! --------------------------------------------------------------------------

    READ(5,'(A)') firstline
    READ(firstline,*,iostat=stat) sys , sym
    
    lxyz = .FALSE.
    IF( sys == 'M' .OR. sys == 'm' .OR. sys == 'P' .OR. sys == 'p' ) lxyz = .TRUE.

    CALL freq_read( 5 , freq , stat )

    ALLOCATE( atm(0) )
    IF( lxyz ) CALL xyz_read( 5 , atm , stat )

    ! conversion bar to Pascal
    mweight = 0.0D0
    DO i = 1,SIZE(atm)
      ! molecule weight
      mweight = mweight + atm(i)%m
    ENDDO

    IF( lxyz ) CALL calc_trans( mweight , temp , pres , etrans , ctrans , strans )
    IF( lxyz ) CALL calc_rot( atm  , sym , temp , erot , crot , srot )

    CALL calc_vib( freq , temp , evib , cvib , svib , zpe , shift )

    ! --------------------------------------------------------------------------
    ! calculate enthalpy, entropy, and gibbs energy correction
    ! --------------------------------------------------------------------------
    enthalpy = etrans + erot + evib
    heatcap  = ctrans + crot + cvib
    entropy  = strans + srot + svib
    gibbs    = enthalpy - entropy*temp

  END SUBROUTINE calc_thermo

  SUBROUTINE zero_data()
    etrans   = 0.0D0
    ctrans   = 0.0D0
    strans   = 0.0D0
    erot     = 0.0D0
    crot     = 0.0D0
    srot     = 0.0D0
    enthalpy = 0.0D0
    heatcap  = 0.0D0
    entropy  = 0.0D0
    gibbs    = 0.0D0
  END SUBROUTINE zero_data

END PROGRAM therm
