BIN=~/.local/bin
LIBEXEC=$(BIN)

# flags
FC      = gfortran
FFLAGS  = -O2 -std=f2003

all: therm

#therm: dsyevj3.o thermlib.o therm.o
therm: dsyevj3.o therm.o
	$(FC) $(FFLAGS) -o $@ $^

install: all
	if [ ! -d ${BIN} ]; then mkdir -p ${BIN}; fi; \
	if [ ! -d ${LIBEXEC} ]; then mkdir -p ${LIBEXEC}; fi; \
	#sed -e 's#BIN=.#BIN=${LIBEXEC}#g' sthermo >${BIN}/sthermo; \
	cp therm ${LIBEXEC};\
	cp mod-hess-freqs ${LIBEXEC}
	cp mol-hess-freqs ${LIBEXEC}

%.o: %.f90
	$(FC) $(FFLAGS) -c $< -o $@

%.o: %.f
	$(FC) $(FFLAGS) -c $< -o $@

clean:
	rm -f $(EXE) *.o *.mod
